package ee.sda.seven.constant;

public class ConstantExample {

    // Constant is the value that never changes.
    // Number of days in a week

    // static makes your field common for your object
    // final makes your field unchangeable
    // they together makes constant
    public final static int daysInWeek = 7;

    public final static String PI = "3.145";
    // You can put your constant here

    // TODO 3: Think of any constant in real life and try to create it in Java
    public static void main(String[] args) {


    }
}
