package ee.sda.seven.datastructures;

/**
 * Array:
 *
 * 12, 56, 76, 89, 100, 343, 21, 234 -> 77, 88, 25
 *
 * Stack:
 *
 * Add(77, 88, 25)
 * 25, 88, 77, 12, 56, 76, 89, 100, 343, 21 234
 *
 * Remove(88)
 *
 *  25, 77, 12, 56, 76, 89, 100, 343, 21 234
 *
 *  First in, last out
 */

// TODO Homework, try to think any real world example about usage of stack (First in last out)
public class StackExample {

    private int[] array;
    private int numberOfElements;
    private int max;

    public StackExample(int max) {
        this.array = new int[max];
        this.numberOfElements = 0;
        this.max = max;
    }

    public void push(int newElement){

        if(numberOfElements >= max) {
            System.out.println("Sorry, stack is full");
        } else {
            array[numberOfElements] = newElement;
            numberOfElements++;
        }
    }

    public int pop() {

        if(numberOfElements == 0){
            System.out.println("Sorry, stack is empty and we can not remove");

            return 0;
        } else {
            numberOfElements--;

            return array[numberOfElements];
        }
    }

    //Push -> adding an element to the stack
    //Pop -> Removing/getting.

    public static void main(String[] args) {

        StackExample newStack = new StackExample(40);
        //12, 56, 76, 89, 100, 343, 21, 234

        newStack.push(234);
        newStack.push(21);
        newStack.push(343);
        newStack.push(100);
        newStack.push(89);
        newStack.push(76);
        newStack.push(56);
        newStack.push(12);

        System.out.println("The element taken from the top: " +newStack.pop());
        System.out.println("Take another element: " +newStack.pop());
        System.out.println("Take yet another element: " +newStack.pop());

    }
}
