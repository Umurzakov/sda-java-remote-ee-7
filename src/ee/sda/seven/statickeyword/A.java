package ee.sda.seven.statickeyword;

public class A {
    private static int a = 0;

    public static void main(String[] args) {
        int a = 5;
        System.out.println(f(a)); // 6

        // a = 6?
        System.out.println(f(a));
        // a = 7

        System.out.println(a);
        // a = 5, why not a = 8?
    }

    public static int f(int x) {
        // x = 5;
        a++;

        // a = 1;
        return a + x;
    }
}

