package ee.sda.seven.methods;

import ee.sda.seven.constant.ConstantExample;

public class MethodsExample {

    // Method main
    public static void main(String[] args) {

        MethodsExample anyName = new MethodsExample();

        int result = anyName.sumTwoNumbers(6, 7);

        System.out.println(result);

        // 3 * (4 + 2)

        int result2 = 3 * result;

        System.out.println(result2);
    }

    // Method for finding sum of two numbers
    public int sumTwoNumbers(int a, int b){
        // a and b can be any number like 4 and 2
        int sum = a + b;

        return sum;
    }
}
