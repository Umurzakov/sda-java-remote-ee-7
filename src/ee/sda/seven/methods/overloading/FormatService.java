package ee.sda.seven.methods.overloading;

public class FormatService {

    public String formatNumber(int value){
        return String.valueOf(value) + " years";
    }

    public String formatNumber(double value){
        return String.valueOf(value) + " EUR";
    }
}
