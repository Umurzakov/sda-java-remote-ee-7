package ee.sda.seven.methods.overloading;

public class Adder {

    public int add(int a, int b){
        return a + b;
    }

    // Method overloading
    public int add(int a, int b, int c){
        return a + b + c;
    }

    public int add(int a, int b, int c, int d){
        return a + b + c + d;
    }

    public double add(int a, int b, int c, double d){
        return a + b + c + d;
    }
}
