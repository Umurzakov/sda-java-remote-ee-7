package ee.sda.seven.objectandclasses;

public class Student {
    // Each class has some members:

    // Members are:
    // Fields
    // Methods
    // Constructor

    // Constructor example
    // It has no return
    // The name is the same as class Name
    public Student(String id){
        this.id = id;
    }


    // States or fields

    // Optional fields
    public String firstName;
    public String lastName;
    // Required field
    private String id;
    public String faculty;
    private int age;

    // Methods or behaviors
    public void study(){

        System.out.println("Student number " +id + " or " +firstName +" is studying");
    }

    public void setId(String id) {

        this.id = id;
    }

    public String getId(){
        return id;
    }

    public static void main(String[] args) {

        // Template
        Student student1 = new Student("12345");

        student1.firstName = "John";
        student1.lastName = "Smith";
        student1.faculty = "Computer science";

        Student student2 = new Student("12346");

        student2.firstName = "Alina";
        student2.lastName = "Smith";
        student2.faculty = "Computer science";
        student2.age = 24;

        student1.study();

        student2.study();
    }
}
