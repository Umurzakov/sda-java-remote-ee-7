package ee.sda.seven.objectandclasses;

public class AtmExample {

    private String atmId;
    private double amount;
    private double[] accountMoney;

    public AtmExample(String atmId){
        this.atmId = atmId;
        this.amount = 0;
        this.accountMoney = new double[10000];
    }

    public void insert(double newAmount){
        this.amount = amount + newAmount;
    }

    public void deposit(double someMoney, int accountId){
        // Do some operation...
        this.amount = amount + someMoney;

        accountMoney[accountId] = accountMoney[accountId] + someMoney;

        System.out.println(someMoney
                + " is deposited to given account id " +accountId);
    }

    //TODO: Homework 2
    // Complete withdraw method by checking cases like
    // what happens if there is no money to withdraw from ATM
    // Tips:
    // There are two cases should be checked
    public void withdraw(double someMoney, int accountId){

        // One case for the validation
        if(this.amount < 0) {
            System.out.println("The ATM machine has not enough money to withdraw");
        }

        //

        // Do some operation...
        this.amount = amount - someMoney;

        accountMoney[accountId] = accountMoney[accountId] - someMoney;

        System.out.println(someMoney
                + " is withdrawn from given account id " +accountId);
    }

    public void checkBalance(int accountId){

        System.out.println("You have this amount in total: " +accountMoney[accountId]);
    }

    public void displayAtmBalance(){
        System.out.println("ATM has this amount left: "+ amount);
    }
}
