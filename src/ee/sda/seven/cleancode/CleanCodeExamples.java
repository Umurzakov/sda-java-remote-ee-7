package ee.sda.seven.cleancode;

public class CleanCodeExamples {

    public static void main(String[] args) {
        CleanCodeExamples cleanCodeExamples= new CleanCodeExamples();

        String newName = cleanCodeExamples.updateFullName("New name");

        cleanCodeExamples.printFullName(newName);

    }

    // 1. DRY - Don't repeat yourself
    // 2. KISS - Keep it simple and stupid
    // 3. YAGNI - You are not gonna use it

    /**
     * Prints full name of an employee
     *
     * @param firstName first name of an employee
     * @param lastName last name of an employee
     */
    public void printFullName(String firstName, String lastName){

        System.out.println("Your full name is " +firstName +" "+lastName);
    }

    public void printFullName(String fullName){

        System.out.println("Your full name is " +fullName);
    }

    public String updateFullName(String newFullName){
        return newFullName;
    }

}
