package ee.sda.seven.fundamentalscoding;


import java.util.Scanner;

/**
 * Anagrams mean if two Strings have the same characters but in a different order.
 * For example: Angel and Angle are anagrams
 */
public class StringAnagramExample {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Write the first string:");

        String firstString = scanner.next();

        System.out.println("Write the second string");

        String secondString = scanner.next();

        System.out.println("Given strings are anagrams: " + IsAnagram(firstString, secondString));
    }

    // TODO Homework 1: PLease, try to find issues with this method implementation
    // And fix them
    public static boolean IsAnagram(String firstString, String secondString){

        if(firstString.length() != secondString.length()){
            return false;
        }

        for (int i = 0; i < firstString.length(); i++) {

            char letter = firstString.charAt(i);

            int index = secondString.indexOf(letter);

            if(index == -1){
                return false;
            }
        }

        return true;
    }
}
