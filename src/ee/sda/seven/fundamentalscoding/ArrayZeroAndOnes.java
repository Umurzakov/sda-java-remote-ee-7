package ee.sda.seven.fundamentalscoding;

import java.util.Arrays;

/**
 *
 * Given an array of 0’s and 1’s in random order,
 * you need to separate 0’s and 1’s in an array.
 *
 * Input:
 * 0,1,0,0,1,1,1,0,1
 *
 * 0,0,0,0,1,1,1,1,1
 *
 * 2,2,2,2,4,4,4,4,4
 *
 * Output
 * 0,0,0,0,1,1,1,1,1
 */
public class ArrayZeroAndOnes {

    public static void main(String[] args) {

        int[] input = {0,1,0,0,1,1,1,0,1};

        System.out.println("Array after separating odd and even numbers: ");

        int[] result = separateZerosAndOnes(input, 2, 4);

        System.out.println(Arrays.toString(result));
    }

    // TODO Homework:
    // Input:
    // ababbbbaaaabbbb

    // Output:
    // aaaaaabbbbbbbbb

    // Hint: You can use char[]
    public static int[] separateZerosAndOnes(int[] input,
                                             int number1,
                                             int number2){

        int twosCounter = 0;

        for (int i = 0; i < input.length; i++) {
            if(input[i] == number1) {
                twosCounter++;
            }
        }

        for (int i = 0; i < twosCounter; i++) {
            input[i] = number1;
        }

        for (int i = twosCounter; i < input.length; i++) {
            input[i] = number2;
        }

        return input;
    }
}
