package ee.sda.seven.fundamentalscoding;

import java.util.Arrays;

/**
 *
 * You are given arrival and departure time of trains reaching to a particular station.
 * You need to find minimum number of platforms required
 * to accommodate the trains at any point of time.
 *
 * arr[] = {100, 140, 150, 200, 215, 400}
 * dep[] = {110, 300, 220, 230, 315, 600}
 */
public class MinimumNumberOfPlatforms {

    public static void main(String[] args) {

        int[] arr = {100, 140, 150, 200, 215, 400};
        int[] dep = {110, 300, 220, 230, 315, 600};

        /** T      A         P
         *  1:00  Arrival   1
         *  1:10  Departure 0
         *  1:40  Arrival   1
         *  1:50  Arrival   2
         *  2:00  Arrival   3
         *  2:15  Arrival   4
         *  2:20  Departure 3
         *
         *  .................
         */

        System.out.println("The minimum number of platforms needed is: "
        +findMinPlatformsRequiredForStation(arr, dep));
    }

    // TODO Homework: Print to the console at what time needed max number of platforms
    // The minimum number of platforms needed is: 4
    // At this time was max platform: ?
    public static int findMinPlatformsRequiredForStation(int[] arrivalTimes,
                                                         int[] departureTimes){
        int minPlatformsNeeded = 0;
        int maxPlatformsNeeded = 0;

        Arrays.sort(arrivalTimes);
        // 100, 140, 150, 200, 215, 400
        Arrays.sort(departureTimes);

        int i = 0, j = 0;

        int time = 0;
        while (i < arrivalTimes.length && j < departureTimes.length){

            // This is arrival time
            if(arrivalTimes[i] < departureTimes[j]){

                minPlatformsNeeded++;


                if(minPlatformsNeeded > maxPlatformsNeeded){
                    maxPlatformsNeeded = minPlatformsNeeded;
                    // The time also update
                    time = arrivalTimes[i];
                    //put debug point

                }
                i++;
            }
            // This is departure time
            else {

                minPlatformsNeeded--;
                j++;
            }
        }

        System.out.println("At this time was max platform: " +time);

        return maxPlatformsNeeded;
    }
}
