package ee.sda.seven.innerclasses;

public class InnerClassExample {

    public static void main(String[] args) {

        PersonalComputer pc = new PersonalComputer();

        pc.setPrice(1200);

        PersonalComputer.CPU cpu = pc.new CPU();
        cpu.cores = 7;
        cpu.manufacturer = "Intel Core";

        PersonalComputer.RAM ram = pc.new RAM();
        ram.clockSpeed = 2.2;
        ram.memory = 16; // 16 GB

        PersonalComputer.SSD ssd = pc.new SSD();
        ssd.memorySize = 500;

        System.out.println("Ram clock speed = " +ram.clockSpeed);
    }
}


